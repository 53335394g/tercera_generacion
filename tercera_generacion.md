# Generacions del llenguatges de programació

![foto](https://lh3.googleusercontent.com/-gPclgNhF0tY/TXhz96TzfiI/AAAAAAAAABY/Whjo3TjBFsk/s1600/ESTRUCTURA.bmp)
___
## **En què es caracteritzen?**

Són llenguatges  molt més independents de la màquina i més programables.
Inclouen funcions com la compatibilitat millorada dels tipus de dades agregades i l'expressió de conceptes de manera que afavoreixi el programador i no l'ordinador. 
Un llenguatge de tercera generació millora el llenguatge de segona generació, ja que l'equip s'encarrega de detalls no essencials. 

## Cap a quins sectors estan destinats?

Aquests llenguatges estan destinats a especialistes, programadors i ambients en què calgui desenvolupar programes i sistemes que requereixen un procediment específic per a l'ordinador.


## **Quins llenguatges hi podem trobar?**

Els primers llenguatges de tercera generació es van veure per primera vegada a finals de la dècada de 1950.
**Alguns exemples son: Fortran, ALGOL i COBOL**

En la actualitat podem trobar llenguatges com:**C, Fortran, Smalltalk, Ada, C ++, C #, Cobol, Delphi, Java, PHP, etc.**

![foto](https://weeklysqueak.files.wordpress.com/2008/10/classesperspective.jpg)
![foto](http://www.codingdojo.com/blog/wp-content/uploads/6.jpg)

```fortran
C AREA OF A TRIANGLE - HERON'S FORMULA
C INPUT - CARD READER UNIT 5, INTEGER INPUT
C OUTPUT -
C INTEGER VARIABLES START WITH I,J,K,L,M OR N
      READ(5,501) IA,IB,IC
  501 FORMAT(3I5)
      IF(IA.EQ.0 .OR. IB.EQ.0 .OR. IC.EQ.0) STOP 1
      S = (IA + IB + IC) / 2.0
      AREA = SQRT( S * (S - IA) * (S - IB) * (S - IC) )
      WRITE(6,601) IA,IB,IC,AREA
  601 FORMAT(4H A= ,I5,5H  B= ,I5,5H  C= ,I5,8H  AREA= ,F10.2,
     $13H SQUARE UNITS)
      STOP
      END
```


